# AppImage模板 #

- 自带Alpine环境

- Linux全平台支持

结构
---

#### 模板结构 ####

![structure](structure.png)


- **app.dir**
    对应的是软件目录，包含Alpine环境

- **run.sh**
    软件入口

- **AppRun**
    AppImage程序入口

- **icon.dir**
    图标目录

- **icon.png**
    软件图标，格式可以是png, svg, xpm等

其中app.dir因为目录比较复杂所以截图里并没有详细列出子目录，我们只需要关注其中的run.sh文件即可。


#### AppImage结构 ####

- Runtime

- SquashFS

启动流程
---

- Runtime

- AppRun

- app.dir/run.sh


1. AppImage从**Runtime**启动以后第一个入口是SquashFS文件系统中的**AppRun**
  **AppRun**主要用来做一些运行前的准备，可以是脚本按理也可以是二进制文件。

2. **app.dir/run.sh**是启动以后的第二个入口，正常流程中这一步也可以直接就是主程序
  在本模板中我们使用**AppRun**作为设置环境变量的脚本以完成将app.dir目录作为系统的扩展。

使用方法
---

1. 将需要制作成AppImage的软件放置在app.dir对应目录。

2. 修改Template.dekstop为<软件名>.desktop并修改对应的信息。

3. 尝试使用./AppRun启动软件并并依据报错提示修复依赖。

#### 依赖修复 ####

使用命令proot或者设置环境变量的方法在app.dir下使用apk命令安装需要的依赖文件即可。

注意事项
---

- 自带环境为Alpine3.7

- 不建议修改AppRun文件


